# NixOS ISO for installations.
# Build with:
#   nix-build '<nixpkgs/nixos>' -A config.system.build.isoImage -I nixos-config=iso.nix

{ lib, ... }:

with lib;

{

  imports = [
    ../modules/base.nix
    <nixpkgs/nixos/modules/installer/cd-dvd/installation-cd-minimal.nix>
    <nixpkgs/nixos/modules/installer/cd-dvd/channel.nix>
  ];

  system.hasUtils = true;

  networking.wireless.enable = true;

  services.openssh.enable = true;
  services.openssh.ports = 22;
  system.remoteRoot = true;

  # Enable SSH in the boot process, for some reason necessary on the ISO.
  systemd.services.sshd.wantedBy = mkForce [ "multi-user.target" ];

}
