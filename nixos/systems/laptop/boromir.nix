# My laptop config.

{ config, pkgs, ... }:

{

  imports = [
    ../../modules/base.nix
    /etc/nixos/hardware-configuration.nix
  ];

  system.isDesktop = true;
  system.isEncrypted = true;
  system.gpg = {
    useAgent = true;
    useYubikey = true;
  };
  system.isIntel = true;
  system.isUefi = true;
  system.nonRootUser.enable = true;
  security.sudo.wheelNeedsPassword = true;
  system.hasUtils = true;
  system.isHypervisor = true;
  programs.zsh.enable = true;

  system.environment.enableDev = true;
  system.wg.role = "client";

  networking.hostName = "boromir";
  time.timeZone = "Europe/Stockholm";

}
