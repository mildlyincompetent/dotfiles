# Plex logical network specification

{

  network.description = "Plex server.";

  plex =
    { config, lib, pkgs, ... }:

    with lib;

    {
      imports = [ ../../modules/base.nix ];

      system.hasUtils = true;

      time.timeZone = "Europe/London";

      networking = {
        hostName = "plex";
        firewall = {
          enable = true;
          allowedTCPPorts = [ 22 32400 ];
          allowedUDPPorts = [ 32400 ];
        };

        usePredictableInterfaceNames = true;
        interfaces.enp0s3.useDHCP = true;
        interfaces.enp0s4.ipv4.addresses = [{
          address = "95.154.197.129";
          prefixLength = 24;
        }];

        defaultGateway = "95.154.197.1";
        nameservers = [
          "1.1.1.1"
          "8.8.8.8"
          "8.8.4.4"
        ];
      };

      services.openssh.enable = true;
      system.isNixOps = true;

      virtualisation.docker.enable = true;
      environment.systemPackages = [ pkgs.plex-docker ];

    };

}
