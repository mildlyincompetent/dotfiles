# Plex physical network specification

{

  plex =
    {config, lib, pkgs, ...}:

    with lib;

    {
      deployment.targetEnv = "libvirtd";
      deployment.libvirtd.memorySize = 8192;
      deployment.libvirtd.vcpu = 12;
      deployment.libvirtd.baseImageSize = 2048;
      deployment.libvirtd.networks = [
        "mgmt"
        "exposed"
      ];
      deployment.targetPort = 22;
      networking.publicIPv4 = "95.154.197.129";
    };

}
