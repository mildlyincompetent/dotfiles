# Mundus physical specification

{ config, lib, pkgs, ... }:

with lib;

{

  deployment.targetEnv = "libvirtd";
  deployment.libvirtd = {
    memorySize = 16384;
    vcpu = 12;
    baseImageSize = 256;
    networks = [
      "mgmt"
      "dev"
    ];
  };

  deployment.targetPort = 22;

}
