# Dev environment

{

  network.description = "Dev environment.";

  mundus = import ./mundus.nix;

  oblivion = import ./oblivion.nix;

}
