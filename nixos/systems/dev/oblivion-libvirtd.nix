# Oblivion physical specification

{ config, lib, pkgs, ... }:

with lib;

{

  deployment.targetEnv = "libvirtd";
  deployment.libvirtd.memorySize = 1024;
  deployment.libvirtd.vcpu = 4;
  deployment.libvirtd.baseImageSize = 25;
  deployment.libvirtd.networks = [
    "mgmt"
    "dev"
    "exposed"
  ];
  deployment.targetPort = 22;

  deployment.keys.wg-priv = {
    text = (import ../../../private/wireguard/dev.priv.nix);
    user = "root";
    group = "root";
    permissions = "0400";
  };

}
