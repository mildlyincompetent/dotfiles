# Mundus - primary dev server

{ config, lib, pkgs, ... }:

with lib;

{

  imports = [ ../../modules/base.nix ];

  time.timeZone = "Europe/London";

  networking = {
    hostName = "mundus";
    firewall = { enable = true; allowedTCPPorts = [ 22 ]; };

    usePredictableInterfaceNames = true;
    interfaces.enp0s3.useDHCP = true;
    interfaces.enp0s4.ipv4.addresses = [{
      address = "172.30.1.2";
      prefixLength = 16;
    }];

    defaultGateway = "172.30.0.1";
    nameservers = [
      "1.1.1.1"
      "8.8.8.8"
      "8.8.4.4"
    ];

  };

  services.openssh.enable = true;
  programs.mosh.enable = true;
  system.isNixOps = true;

  system.environment.enableDev = true;
  system.wg.role = "lan";
  system.wg.lanInterface = "enp0s4";
  system.gitViaLab = true;

  system.hasUtils = true;
  system.nonRootUser.enable = true;
  programs.zsh.enable = true;

}
