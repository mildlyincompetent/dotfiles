# Dev environment physical specification.

{

  mundus = import ./mundus-libvirtd.nix;

  oblivion = import ./oblivion-libvirtd.nix;

}
