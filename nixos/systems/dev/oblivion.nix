# Oblivion - dev environment wireguard server.

{ config, lib, pkgs, ... }:

with lib;

{
  imports = [ ../../modules/base.nix ];

  system.hasUtils = true;

  time.timeZone = "Europe/London";

  networking = {
    hostName = "oblivion";
    firewall.enable = true;
    firewall.allowedTCPPorts = [ 22 ];

    usePredictableInterfaceNames = true;
    interfaces.enp0s3.useDHCP = true;
    interfaces.enp0s4.ipv4.addresses = [{
      address = "172.30.1.1";
      prefixLength = 16;
    }];
    interfaces.enp0s5.ipv4.addresses = [{
      address = "95.154.197.130";
      prefixLength = 24;
    }];

    defaultGateway = "95.154.197.1";
    nameservers = [
      "1.1.1.1"
      "8.8.8.8"
      "8.8.4.4"
    ];
  };

  services.openssh.enable = true;
  system.isNixOps = true;

  system.environment.enableDev = true;
  system.wg.role = "server";

}
