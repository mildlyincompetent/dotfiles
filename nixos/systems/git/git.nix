# Git server

{

  network.description = "Git server.";

  git =
    { config, lib, pkgs, ... }:

    with lib;

    {
      imports = [
        ../../modules/base.nix
      ];

      system.hasUtils = true;

      time.timeZone = "Europ/London";

      networking = {
        hostName = "git";
        firewall = {
          enable = true;
          allowedTCPPorts = [ 22 80 443 3000 ];
        };

        usePredictableInterfaceNames = true;
        interfaces.enp0s3.useDHCP = true;
        interfaces.enp0s4.ipv4.addresses = [{
          address = "95.154.197.132";
          prefixLength = 24;
        }];
        interfaces.enp0s5.ipv4.addresses = [{
          address = "172.30.1.3";
          prefixLength = 16;
        }];

        defaultGateway = "95.154.197.1";

        nameservers = [
          "1.1.1.1"
          "8.8.8.8"
          "8.8.4.4"
        ];
      };

      services.openssh.enable = true;
      system.remoteRoot = true;

      system.environment.enableDev = true;
      system.wg.role = "lan";
      system.wg.lanInterface = "enp0s5";

      virtualisation.docker.enable = true;
      environment.systemPackages = [ pkgs.gitea-docker ];

      # services.openssh.ports = [ 22 ]; 
    };

}
