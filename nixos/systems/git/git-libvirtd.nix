# Git server physical spec.

{

  git =
    { config, lib, pkgs, ... }:

    with lib;

    {
      deployment.targetEnv = "libvirtd";
      deployment.libvirtd = {
        memorySize = 4096;
        vcpu = 6;
        baseImageSize = 256;
        networks = [
          "mgmt"
          "exposed"
          "dev"
        ];
      };
      # deployment.targetPort = 22;
      deployment.targetPort = 31708;
    };

}
