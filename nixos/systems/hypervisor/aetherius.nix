# My hypervisor config.

{ config, lib, pkgs, ... }:

with lib;

{

  imports = [
    ../../modules/base.nix
    /etc/nixos/hardware-configuration.nix
  ];

  system.isEncrypted = true;
  system.isIntel = true;
  system.isUefi = true;
  system.nonRootUser.enable = true;
  system.hasUtils = true;
  system.isHypervisor = true;

  programs.mosh.enable = true;
  programs.zsh.enable = true;
  services.openssh.enable = true;

  time.timeZone = "Europe/London";

  powerManagement.cpuFreqGovernor = "performance";

  system.gitViaLab = true;

  networking = {
    hostName = "aetherius";
    firewall.enable = true;
    firewall.checkReversePath = false;

    usePredictableInterfaceNames = true;
    interfaces.eno1.ipv4.addresses = [{
      address = "95.154.197.127";
      prefixLength = 24;
    }];
    defaultGateway = "95.154.197.1";
    nameservers = [
      "1.1.1.1"
      "8.8.8.8"
      "8.8.4.4"
    ];
  };

}
