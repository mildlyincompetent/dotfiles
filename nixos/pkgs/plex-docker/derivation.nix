{ coreutils, docker, writeShellScriptBin }:

writeShellScriptBin

"plex-docker"

''
  ${docker}/bin/docker run \
  -d \
  --name plex \
  --network=host \
  -e TZ="Europe/London" \
  -v /plex/config:/config \
  -v /plex/transcode:/transcode \
  -v /plex/data:/data \
  plexinc/pms-docker:plexpass
''
