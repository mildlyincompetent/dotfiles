{ coreutils, gnupg, writeShellScriptBin, writeText }:

let
  file = import ./kch-pgp-file.nix writeText;
in

writeShellScriptBin

"kch-pgp"

''
  ${coreutils}/bin/cat ${file} | ${gnupg}/bin/gpg --import
''
