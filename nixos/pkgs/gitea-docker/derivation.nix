{ coreutils, docker, writeShellScriptBin }:

writeShellScriptBin

"gitea-docker"

''
  ${coreutils}/bin/mkdir -p /gitea
  ${coreutils}/bin/chown -R 1000:1000 /gitea
  ${docker}/bin/docker run \
  -d \
  --name gitea \
  -v /gitea:/data \
  -p 22:22 \
  -p 80:10080 \
  -p 443:10443 \
  -e USER_UID="1000" \
  gitea/gitea:latest
''
