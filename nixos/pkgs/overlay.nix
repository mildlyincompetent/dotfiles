self: super:

{
  kch-pgp = super.callPackage ./kch-pgp/derivation.nix {};
  plex-docker = super.callPackage ./plex-docker/derivation.nix {};
  gitea-docker = super.callPackage ./gitea-docker/derivation.nix {};
}
