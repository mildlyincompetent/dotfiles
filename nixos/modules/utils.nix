# Various system utilities.

# These are command-line utilities which are useful to have around but don't
# necessarily require their own module.

{ config, lib, pkgs, ... }:

with lib;

{

  options.system.hasUtils = mkOption {
    type = types.bool;
    default = false;
    description = "Whether to include utility / misc software.";
  };

  config = mkIf config.system.hasUtils {

    # Useful packages
    environment.systemPackages = with pkgs; [
      bash
      bat
      bind
      cowsay
      direnv
      file
      fortune
      git
      git-lfs
      gnupg
      gnutar
      htop
      imagemagick
      kch-pgp
      lolcat
      mosh
      neofetch
      neovim
      nixops
      openssh
      pass
      sl
      stow
      sudo
      tcpdump
      tig
      tmux
      wireguard
      youtube-dl
      zstd
    ];

    # Enable `mtr` (needs SUID / capabilities wrapper).
    programs.mtr.enable = true;

  };

}
