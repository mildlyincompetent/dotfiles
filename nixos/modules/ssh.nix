# SSH config with optional root access.

{ config, lib, pkgs, ... }:

with lib;

let sshKeys = import ./ssh-keys.nix; in

{

  options.system.remoteRoot = mkOption {
    type = types.bool;
    default = false;
    description = "Whether to enable remote root logins.";
  };

  options.system.isNixOps = mkOption {
    type = types.bool;
    default = false;
    description = "Whether this system is provisioned via NixOps.";
  };

  config = mkMerge [

    (mkIf config.services.openssh.enable {
      # Never allow password login.
      services.openssh.passwordAuthentication = false;
      # Use a random port to reduce log spam.
      services.openssh.ports = [ 31708 ];
    })

    (mkIf (config.services.openssh.enable && (!config.system.remoteRoot && !config.system.isNixOps)) {
      # Do not allow remote root access.
      services.openssh.permitRootLogin = "no";
    })

    (mkIf (config.services.openssh.enable && (config.system.remoteRoot || config.system.isNixOps)) {
      # Allow remote root access (though not via password).
      services.openssh.permitRootLogin = mkForce "prohibit-password";
      # Allow my SSH key to connect.
      users.users.root.openssh.authorizedKeys.keys = sshKeys;
    })

    (mkIf (config.services.openssh.enable && config.system.isNixOps) {
      # NixOps doesn't handle non-22 SSH ports well, and it really doesn't
      # provide real security mitigations anyways.
      services.openssh.ports = [ 22 ];
    })

  ];

}
