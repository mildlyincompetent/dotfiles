# Spotify with local network discovery.

{config, lib, pkgs, ...}:

with lib;

{

  config = mkIf config.system.isDesktop {

    environment.systemPackages = [ pkgs.spotify ];
    networking.firewall.allowedTCPPorts = [ 57621 ];

  };

}
