# Desktop device networking setup.

{ config, lib, pkgs, ... }:

with lib;

{

  config = mkIf config.system.isDesktop {
    networking.networkmanager.enable = true;
    environment.systemPackages = [ pkgs.networkmanagerapplet ];
    networking.firewall.enable = true;
  };

}
