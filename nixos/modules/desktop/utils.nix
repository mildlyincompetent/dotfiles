# Generic desktop software and utilities.

{ config, lib, pkgs, ... }:

with lib;

{

  config = lib.mkIf config.system.isDesktop {
    environment.systemPackages = with pkgs; [
      chromium
      discord
      firefox
      gitkraken
      notable
      qtpass
      thunderbird
      vlc
      xclip
    ];
  };

}
