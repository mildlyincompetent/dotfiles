# Bluetooth config. Technically you may want bluetooth in a non-desktop system,
# but it's not too likely for my use cases.

{ config, lib, pkgs, ... }:

with lib;

{

  config = mkIf config.system.isDesktop {

    # Enable bluetooth.
    hardware.bluetooth.enable = true;

    # Use the full pulse audio package to get headset support.
    hardware.pulseaudio = {
      package = pkgs.pulseaudioFull;
    };

    # SSP breaks certain (read: my) headset connectivity.
    systemd.services.disableBluetoothSsp = {
      wantedBy = [ "bluetooth.target" ];
      before = [ "bluetooth.service" ];
      description = "Disable SSP to allow bluetooth headset to properly connect.";
      serviceConfig = {
        Type = "oneshot";
        User = "root";
        ExecStart = ''${pkgs.bluez}/bin/btmgmt ssp off'';
      };
    };

  };

}
