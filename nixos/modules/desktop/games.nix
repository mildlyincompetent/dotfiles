# Gaming related packages and software.

{ config, lib, pkgs, ... }:

with lib;

{

  config = mkIf config.system.isDesktop {

    # 32-bit software needed for Steam.
    hardware.opengl.driSupport32Bit = true;
    hardware.opengl.extraPackages32 = [ pkgs.pkgsi686Linux.libva ];
    hardware.pulseaudio.support32Bit = true;

    environment.systemPackages = with pkgs; [
      steam
      multimc
    ];

  };

}
