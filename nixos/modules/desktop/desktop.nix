# The module used for systems with graphical desktop functionality.

# This module should always be imported as it provides the boolean option which
# determines whether a system is a desktop or not, which may be used by other
# modules down the line to toggle particular features (for example there's not
# much point installing the network manager tray applet if there's no tray, but
# network manager itself can be used fine from the command line).

{ config, lib, pkgs, ... }:

with lib;

{

  imports = [
    ./bluetooth.nix
    ./games.nix
    ./network.nix
    ./spotify.nix
    ./utils.nix
  ];

  options.system.isDesktop = mkOption {
    type = types.bool;
    default = false;
    example = true;
    description = "Whether or not this is a graphical (X11) desktop system.";
  };

  config = mkIf config.system.isDesktop {

      # Enable X11.
      services.xserver.enable = true;
      services.xserver.layout = "gb";

      # Enable KDE.
      services.xserver.displayManager.sddm.enable = true;
      services.xserver.desktopManager.plasma5.enable = true;

      # Enable touchpad support.
      services.xserver.libinput.enable = true;

      # Enable sound. Not strictly a requirement on all desktop systems but a
      # pretty safe bet nonetheless.
      sound.enable = true;
      hardware.pulseaudio.enable = true;

      # Enable printing.
      services.printing.enable = true;

  };

}
