# Shim for git server access within the hypervisor.

# The git server has external connectivity via a macvtap driver. This means that
# neither the hypervisor, nor anything which NATs through the hypervisor can
# access its external public IP. However, they can generally access its internal
# (dev environment) IP just fine. Adding this IP to the local hosts file on
# each machine requiring access to this service is a decent workaround for now.

{ config, lib, pkgs, ... }:

with lib;

{

  options.system.gitViaLab = mkOption {
    type = types.bool;
    default = false;
    description = "Whether to accessthe git server via its dev environment IP.";
  };

  config = mkIf config.system.gitViaLab {
    networking.extraHosts = "172.30.1.3 git.kch.dev";
  };

}
