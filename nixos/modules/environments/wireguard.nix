# Wireguard config.

{ config, lib, pkgs, ... }:

with lib;
with builtins;

let
  cfg = config.system.wg;
  envCfg = config.system.environment;
  clientPrivateKey = "/wg/laptop.priv";
  serverPrivateKey = "/run/keys/wg-priv";
  port = 42664;
in

{

  options.system.wg = {

    role = mkOption {
      type = types.enum [ "client" "server" "lan" ];
      default = "client";
      example = "server";
      description = "What role does this device play in the network.";
    };

    lanInterface = mkOption {
      type = types.nullOr types.str;
      default = null;
      example = "eth0";
      description = "The interface used for the LAN.";
    };

  };

  config = mkMerge [

    (mkIf ((envCfg.enableDev || envCfg.enableLab) && (cfg.role != "lan")) {
      networking.wireguard.enable = true;
      networking.firewall.allowedUDPPorts = [ port ];
    })

    (mkIf (cfg.role == "server") {
      boot.kernel.sysctl."net.ipv4.ip_forward" = 1;
    })

    (mkIf (envCfg.enableDev && cfg.role == "client") {
      networking.wireguard.interfaces.wg-dev = {
        ips = [ "172.30.2.1/16" ];
        listenPort = port;
        privateKeyFile = clientPrivateKey;
        peers = [{
          publicKey = "grzTxwNlXR+3gutlB3uJdt2L3+z9H2mcTGtcq7EdfjA=";
          allowedIPs = [ "172.30.0.0/16" ];
          endpoint = "oblivion.kch.dev:${toString port}";
        }];
      };
    })

    (mkIf (envCfg.enableDev && cfg.role == "server") {
      networking.wireguard.interfaces.wg-dev = {
        ips = [ "172.30.1.1/16" ];
        listenPort = port;
        privateKeyFile = serverPrivateKey;
        peers = [{
          publicKey = "IaZTq7kLIvlr/d1uRxj45r8Xo21oRkX4IlP3FkDb63c=";
          allowedIPs = [ "172.30.2.1/32" ];
        }];
      };
    })

    (mkIf (envCfg.enableDev && cfg.role == "lan") {
      networking.interfaces."${cfg.lanInterface}".ipv4.routes = [{
        address = "172.30.2.0"; prefixLength = 24; via = "172.30.1.1";
      }];
    })

  ];

}
