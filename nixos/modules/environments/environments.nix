# Environment configuration

# I run two environments for most of my personal services; a "dev" and a "lab"
# environment. These are each accessible to devices via a wireguard VPN, and
# host various internal services.

{ config, lib, pkgs, ... }:

with lib;

let
  cfg = config.system.environment;
in

{

  imports = [
    ./git.nix
    ./wireguard.nix
  ];

  options.system.environment = {

    enableDev = mkOption {
      type = types.bool;
      default = false;
      description = "Whether this device is in the dev environment.";
    };

    enableLab = mkOption {
      type = types.bool;
      default = false;
      description = "Whether this device is in the lab envrionment.";
    };

  };

}
