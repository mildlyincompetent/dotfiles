# Base configuration.

# This doesn't really do much, as it exclusively includes the things I would
# want in literally every single NixOS deployment. See `utils.nix` for basic
# utilities that would be included in every debuggable / user-friendly NixOS
# deployment, but not necessarily every single NixOS deployment (e.g. NixOS
# in a container may not make use of `utils.nix`, but will use `base.nix`).

{ ... }:

{

  # Some imports. Note that none of these should on their own add packages or
  # significantly fiddle with the system state, although all add functionality.
  imports = [
    # This doesn't actually enable the desktop components unless the appropriate
    # config value is set, and the same goes for the other imports.
    ./desktop/desktop.nix
    ./encrypt.nix
    ./environments/environments.nix
    ./gpg.nix
    ./intel.nix
    ./ssh.nix
    ./uefi.nix
    ./user.nix
    ./utils.nix
    ./virtualisation.nix
    ./zsh.nix
  ];

  # Add our custom package overlay. Note that this does not actually do
  # anything in practice unless we end up using one of our custom packaeges, so
  # no container size bloat will occur, for example.
  nixpkgs.overlays = [ (import ../pkgs/overlay.nix) ];

  # Set internationalisation properties. These are currently consistent across
  # the systems I use.
  i18n = {
    consoleFont = "Lat2-Terminus16";
    consoleKeyMap = "uk";
    defaultLocale = "en_US.UTF-8";
  };

  # I sometimes use non-free packages, and it's easiest to just define this here
  # rather than every time I want to make use of a non-free package.
  nixpkgs.config.allowUnfree = true;

  # Do not use DHCP by default.
  networking.useDHCP = false;

  # We're currently running NixOS 19.09. In some testing environments this may
  # be different, but all "production" deploys will have the same consistent
  # NixOS version. Note that certain packages may well use custom pinned
  # `nixpkgs` versions, but these are not relevant to the whole system's
  # version.
  system.stateVersion = "19.09";

}
