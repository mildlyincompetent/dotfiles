# Non-root user config.

{ config, lib, pkgs, ... }:

with lib;

let
  cfg = config.system.nonRootUser;
  sshKeys = import ./ssh-keys.nix;
in

{

  options.system.nonRootUser = {

    enable = mkOption {
      type = types.bool;
      default = false;
      description = "Whether the system has a non-root user account.";
    };

    userName = mkOption {
      type = types.str;
      default = "kch";
      description = "The username of the non-root user account.";
    };

  };

  config = mkMerge [

    (mkIf cfg.enable {
      users.users."${cfg.userName}" = {
        isNormalUser = true;
        group = "users";
        uid = 1000;
        home = "/home/kch";
        extraGroups = [ "wheel" ];
      };
      security.sudo.wheelNeedsPassword = mkDefault false;
    })

    (mkIf (cfg.enable && config.system.isDesktop) {
      users.users."${cfg.userName}" = {
        extraGroups = [ "video" "audio" "networkmanager" ];
      };
    })

    (mkIf (cfg.enable && config.system.isHypervisor) {
      users.users."${cfg.userName}" = {
        extraGroups = [ "libvirtd" ];
      };
    })

    (mkIf (cfg.enable && config.programs.zsh.enable) {
      users.users."${cfg.userName}" = {
        shell = pkgs.zsh;
      };
    })

    (mkIf (cfg.enable && config.services.openssh.enable) {
      users.users."${cfg.userName}" = {
        openssh.authorizedKeys.keys = sshKeys;
      };
    })

  ];

}
