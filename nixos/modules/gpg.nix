# GPG and smartcard related configuration.

{ config, lib, pkgs, ... }:

with lib;

{

  options.system.gpg = {

    useAgent = mkOption {
      type = types.bool;
      default = false;
      example = true;
      description = "Use GPG agent instead of SSH agent.";
    };

    useYubikey = mkOption {
      type = types.bool;
      default = false;
      example = true;
      description = "Use Yubikey (a GPG smartcard, among other things).";
    };

  };

  config = mkMerge [

    (mkIf config.system.gpg.useAgent {
      # Disable the default ssh agent.
      programs.ssh.startAgent = false;
      # Enable the gpg agent.
      programs.gnupg.agent = {
        enable = true;
        enableSSHSupport = true;
        enableExtraSocket = true;
      };
    })

    (mkIf config.system.gpg.useYubikey {
      # Enable pcscd for USB smartcard functionality.
      services.pcscd.enable = true;
      # Add udev rules for the Yubikey.
      services.udev.packages = with pkgs; [
        yubikey-personalization
        libu2f-host
      ];
      # Add command line yubikey manager.
      environment.systemPackages = with pkgs; [
        yubikey-manager
      ];
    })

    (mkIf (config.system.gpg.useYubikey && config.system.isDesktop) {
      # Add the graphical yubikey manager.
      environment.systemPackages = with pkgs; [
        yubikey-manager-qt
      ];
    })

  ];

}
