# Intel CPU specific config.

{ config, lib, pkgs, ... }:

with lib;

{

  options.system.isIntel = mkOption {
    type = types.bool;
    default = false;
    example = true;
    description = "Whether or not an Intel CPU is in use.";
  };

  config = mkMerge [

    (mkIf config.system.isIntel {
      # Update Intel microcode.
      hardware.cpu.intel.updateMicrocode = true;
    })

  ];

}
