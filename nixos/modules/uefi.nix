# UEFI boot options.

{ config, lib, pkgs, ... }:

with lib;

{

  options.system.isUefi = mkOption {
    type = types.bool;
    default = false;
    description = "Whether this system boots in UEFI mode.";
  };

  config = mkIf config.system.isUefi {
    boot.loader.systemd-boot.enable = true;
    boot.loader.efi.canTouchEfiVariables = true;
  };

}
