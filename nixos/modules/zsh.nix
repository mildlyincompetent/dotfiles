# Zshell configuration.

{ config, lib, pkgs, ... }:

with lib;

{

  config.programs.zsh.ohMyZsh = mkIf config.programs.zsh.enable {
    enable = true;
    theme = "agnoster";
    plugins = [
      "copyfile"
      "dircycle"
      "dirhistory"
      "docker"
      "history"
      "git"
      "pass"
      "sudo"
      "systemd"
      "yarn"
    ];
  };

}
