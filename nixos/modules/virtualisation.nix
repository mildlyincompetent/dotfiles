# Virtualisation / hypervisor configuration.

{ config, lib, pkgs, ... }:

with lib;

{

  options.system.isHypervisor = mkOption {
    type = types.bool;
    default = false;
    example = true;
    description = "Whether this system is a virtualisation hyperisor.";
  };

  config = mkMerge [

    (mkIf config.system.isHypervisor {
      virtualisation.libvirtd.enable = true;
      environment.systemPackages = with pkgs; [
        # VM filesystem utilities.
        libguestfs-with-appliance
        # Windows VirtIO drivers.
        win-virtio
      ];
    })

    (mkIf (config.system.isHypervisor && config.system.isDesktop) {
      environment.systemPackages = with pkgs; [
        # A visual VM manager.
        virtmanager
      ];
    })

    (mkIf (config.system.isHypervisor && config.system.isIntel) {
      # Enable the KVM kernel module.
      boot.kernelModules = [ "kvm_intel" ];
      # Enable nested virtualisation.
      boot.extraModprobeConfig = "options kvm_intel nested=1";
    })

  ];

}
