# Full disk encryption setup.

# I use LVM on LUKS, which is by no means the only valid implementation of full
# disk encryption, but is the one I personally use due to a few nice features
# such as encrypted swap.

{ config, lib, pkgs, ... }:

with lib;

{

  options.system.isEncrypted = mkOption {
    type = types.bool;
    default = false;
    example = true;
    description = "Whether or not this is a full-disk-encrypted system.";
  };

  config = mkMerge [

    (mkIf config.system.isEncrypted {
      # Add kernel modules to make use of hardware accelerated encryption.
      boot.initrd.availableKernelModules = [ "aes_x86_64" "cryptd" ];
      # Mount the LUKS devices before utilising LVM.
      boot.initrd.luks.devices = [
        {
          name = "crypt";
          device = "/dev/disk/by-label/crypt";
          preLVM = true;
        }
      ];
    })

    (mkIf (config.system.isEncrypted && config.system.isIntel) {
      # Hardware AES for faster encryption capability.
      boot.initrd.availableKernelModules = [ "aesni_intel" ];
    })

  ];

}
