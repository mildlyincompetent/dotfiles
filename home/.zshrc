# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=10000
setopt appendhistory autocd extendedglob nomatch notify
unsetopt beep
bindkey -v
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '/home/kch/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall

# Activate direnv
eval "$(direnv hook zsh)"

export EDITOR=nvim

# Function for quickly signing up to a branch with git.
git-multisign () { git rebase --exec 'git commit --amend --no-edit -n -S' -i $1; }

neofetch; fortune | cowsay | lolcat; echo
