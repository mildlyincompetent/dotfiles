call plug#begin('~/.local/share/nvim/plugged')

Plug 'vmchale/dhall-vim'
Plug 'tmhedberg/matchit'
Plug 'sbdchd/neoformat'
Plug 'cakebaker/scss-syntax.vim'
Plug 'LnL7/vim-nix'
Plug 'tpope/vim-sensible'
Plug 'posva/vim-vue'
Plug 'stephpy/vim-yaml'

call plug#end()

set tabstop=2 shiftwidth=2 expandtab number
