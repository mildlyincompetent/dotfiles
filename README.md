# Dotfiles

[![built with nix](https://builtwithnix.org/badge.svg)](https://builtwithnix.org)

This repo contains the configuration of my personal systems and environments.

## WARNING

The setup in this repo works for me, but it might not work for you. Furthermore, the way I use this repo (symlinking bits of /etc/ to this repo) has security implications depending on who owns the files you're symlinking to - do not do this if you don't understand these implications.

## Usage on existing desktop system

Deploy contents of `home` and `private/home` as symlinks relative to your `~` directory. Tooling such as GNU Stow may be useful here.

Symlink `private/passwords` to `.password-store`.

Note: `private` is a submodule with a private remote, for config such as my passwords which I don't want to share with the world.

Symlink `nixos/systems/desktop.nix` to `/etc/nixos/configuration.nix`.

Rebuild NixOS and enjoy!

## Deployment of new desktop systems.

Obtain NixOS, either by downloading an ISO or building `nixos/systems/iso.nix` if you have access to an existing Nix installation.

Partition your system. In my desktop setup I have a LUKS2 parition labelled `crypt` which contains everything but the boot partition. This is fun because it results in encrypted swap, meaning I can hibernate without sacrificing encryption.

Set up your filesystems and generate `hardware-configuration.nix`.

Deploy `nixos/systems/desktop.nix`, with appropriate reference to `hardware-configuration.nix`.

Run `nixos-install` with appropriate arguments (probably `--system`).

Boot into the new system, set your non-root user account password and switch users, and optionally follow the steps to use on an existing system, which you now possess.

## Usage on other systems

The `nixos/systems` folder contains configurations for systems which are not desktop systems, such as hypervisors and virtual machines which I operate. Similar steps to those needed to use NixOS on desktop systems may be taken to deploy NixOS onto these systems.
